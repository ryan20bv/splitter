



//  start for Normal splitter
const basic = document.getElementById('basic')
const basicInfo = document.getElementById('basicInfo')
const billCopy = document.getElementById('billCopy');
const inputBill = document.getElementById('inputBill');
const howMany = document.getElementById('howMany');
const btn = document.getElementById('btn');
const info = document.getElementById('info');



basic.addEventListener('click',function(){
    basicInfo.classList.remove('d-none');
    stretchInfo.classList.add('d-none');
    stressInfo.classList.add('d-none')
})

const basicClear=document.getElementById('basicClear')

basicClear.addEventListener('click',function(){
    location.reload();
    basicInfo.classList.remove('d-none');
    stretchInfo.classList.add('d-none');
    stressInfo.classList.add('d-none')
    
});
    

// start of basic bill splitter
inputBill.addEventListener('keyup', function(){
    billCopy.textContent = inputBill.value;
})
btn.addEventListener('click',function(){
    
    const infoBelow =document.createElement('h3');
    let dividedShare;
    if (inputBill.value <=0 || inputBill.value === ""){
        alert("Input Bill!");
     }else
    if(howMany.value <=0 || howMany.value === ""){
        alert("Input number of person/s!")
    }else{
        info.innerHTML="";
    dividedShare= parseInt(inputBill.value)/parseInt(howMany.value) 

    infoBelow.textContent =+ howMany.value + " " + "persons" + " : " +  dividedShare + " " + "for each " + ", " + "Total " + "= " + (dividedShare*howMany.value)

    info.appendChild(infoBelow);

    }
    
})

// end of basic bill splitter

// start of stretch bill splitter

const stretch = document.getElementById('stretch')
const stretchInfo = document.getElementById('stretchInfo')
const stretchBillCopy = document.getElementById('stretchBillCopy');
const stretchInputBill = document.getElementById('stretchInputBill');
const stretchAddName = document.getElementById('stretchAddName');
const stretchBtn = document.getElementById('stretchBtn');
const stretchDiv = document.getElementById('stretchDiv');

stretch.addEventListener('click', function(){
    basicInfo.classList.add('d-none');
    stretchInfo.classList.remove('d-none')
    stressInfo.classList.add('d-none')
})

const stretchClear=document.getElementById('stretchClear')

stretchClear.addEventListener('click',function(){
    location.reload();
    basicInfo.classList.add('d-none');
    stretchInfo.classList.remove('d-none')
    stressInfo.classList.add('d-none')

});

stretchInputBill.addEventListener('keyup', function(){
    stretchBillCopy.textContent = stretchInputBill.value;
})

let addedNames = []
let stretchDivBelow =""

stretchBtn.addEventListener('click',function(){    
    if (stretchInputBill.value <=0 || stretchInputBill.value === "" && addedNames.length != "0"){
        alert("Input Bill!");
    }else
    if(stretchAddName.value <=0 || stretchAddName.value === "" ){
        if(addedNames.length != "0"){
            billSplitter();
        }else{    
            alert("Add name of person!");
        }
    }else{
    addedNames.push(stretchAddName.value);
    billSplitter();
    }
})

function billSplitter(){
    let calculateAmount="";

    if(addedNames.length === 1){
        calculateAmount = parseInt(stretchInputBill.value);
    }else {
        calculateAmount = parseInt(stretchInputBill.value)/parseInt(addedNames.length);
    }
    
    stretchDiv.innerHTML="";
    addedNames.forEach(function(addedname){
        stretchDivBelow =document.createElement('h3');
        stretchDivBelow.textContent = addedname + " : " +  calculateAmount
        stretchDiv.appendChild(stretchDivBelow);
        
    });
}

function remover(){

}









// end of stretch bill splitter

// start of stress bill splitter


const stress = document.getElementById('stress')
const stressInfo = document.getElementById('stressInfo')

stress.addEventListener('click', function(){
    basicInfo.classList.add('d-none');
    stretchInfo.classList.add('d-none')
    stressInfo.classList.remove('d-none')
})
const stressClear=document.getElementById('stressClear')

stressClear.addEventListener('click',function(){
    location.reload();
    basicInfo.classList.add('d-none');
    stretchInfo.classList.add('d-none')
    stressInfo.classList.remove('d-none')

});


const stressBillCopy = document.getElementById('stressBillCopy');
const stressInputBill = document.getElementById('stressInputBill');
const stressAddName = document.getElementById('stressAddName');
const stressPersonalShare = document.getElementById('stressPersonalShare');
const stressBtn = document.getElementById('stressBtn');
const stressDiv = document.getElementById('stressDiv');

stressInputBill.addEventListener('keyup', function(){
    stressBillCopy.textContent = stressInputBill.value;
})

let stressAddedNames = [];
let stressDivBelow ="";
 

stressBtn.addEventListener('click',function(){ 
    let friend ={
        name: "",
        share: 0,
    }
        friend.name = stressAddName.value;
        friend.share = stressPersonalShare.value;


    if (stressInputBill.value <=0 || stressInputBill.value === "" ){
        alert("Input Bill!");
    }else
    if(stressAddName.value === "" ){
        if(stressAddedNames.length != "0"){
            stressBillSplitter();
        }else{    
            alert("Add name of person!");
        }
    }else if(stressPersonalShare.value < 0 || stressPersonalShare.value === ""){
        alert ("Dont leave the personal share empty")

    } 
    
    else
    
    
    {
          
    stressAddedNames.push({friend});
        // console.log(stressAddedNames)
    stressBillSplitter();
    }
})

function stressBillSplitter(){
    let additionalAmount="";
    let friendShares;
    let friend ={
        name: "",
        share: "",
    }
     
        friend.name = stressAddName.value;
        friend.share = stressPersonalShare.value;
        

    if(stressAddedNames.length === 1){

        additionalAmount = parseInt(stressInputBill.value)-parseInt(friend.share);
        // console.log("im here")
        // console.log(parseInt(friend.share) + parseInt(friend.share))
        
    }else {
        let friendShare = 0;
        stressAddedNames.forEach(function(stressAddedName){
            friendShare += parseInt(stressAddedName.friend.share);
            // console.log(stressAddedName)
            // console.log(parseInt(stressAddedName.friend.share))
            // console.log(friendShare)


        })
        
        
        
        
        additionalAmount = (parseInt(stressInputBill.value)-friendShare)/parseInt(stressAddedNames.length);
        // console.log(stressInputBill.value)
        
        // console.log(stressAddedNames)
        
        // console.log("no i am here")
    }
    
    stressDiv.innerHTML="";
    stressAddedNames.forEach(function(stressAddedName){
        
        stressDivBelow =document.createElement('h3');
        stressDivBelow.textContent = stressAddedName.friend.name + " : " + stressAddedName.friend.share + " + " + additionalAmount;
        stressDiv.appendChild(stressDivBelow);
        
    });
}




// end of stress bill splitter



